"use strict";

// Importing modules used by the server
const express = require("express");
const bodyParser = require("body-parser");
const request = require("request");
const jsonParser = bodyParser.json();

// Initializing the server
let app = express();

// Listening post requests on the route localhost:3002/routes
app.post("/routes", jsonParser, (req, res) => {
    // Returning an error if any body is provided in request
    if (!req.body) {
        return res.status(400).send("Bad Request: no body provided");
    }

    // Getting clients
    const clients = req.body.clients;
    console.log(clients);

    // Returning an error if less than two clients are given in the request body
    if (!clients || !Array.isArray(clients) || clients.length < 2) {
        return res.status(400).send("Bad Request: at least two clients expected");
    }

    // TODO(unknown) call osrm trip service et send result
    // Hint: look at https://github.com/Project-OSRM/osrm-backend/blob/master/docs/http.md#trip-service
    request
        .get("http://router.project-osrm.org/trip/v1/driving/")
        .on("response", (response) => {
            if (response && response.statusCode === 200) {
                console.log(response);
                res.send({});
            }
            else {
                return res.sendStatus(400);
            }
        })
        .on("error", (error) => {
            return res.status(400).send(error);
        });
});

// Initializing port to listen to
app.listen(3002, () => {
    console.log("App listening on port 3002");
});