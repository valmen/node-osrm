## Installation

```bash
npm install
```

## Launch client

```bash
npm start
```

## Launch server

```bash
node server.js
```